from django.conf.urls import url
from .views import *

#url for app, add your URL Configuration

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^login/', auth_login, name='auth_login'),
    url(r'^hapus/$', hapus_data, name='hapus')
]
