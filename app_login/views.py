from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .csui_login_helper import get_access_token, verify_user
import time
from absensi.models import *

response = {}
def index(request):
    if 'user_login' in request.session:
        response['user_login'] = request.session["user_login"]
        return HttpResponseRedirect(reverse('absensi:index'))
    else:
        response['user_login'] = ""
        return render(request, 'login.html', response)

def auth_login(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']

        #request CSUI access token
        access_token = get_access_token(username, password)
        if access_token is not None:
            ver_user = verify_user(access_token)
            kode_identitas = ver_user['identity_number']
            role = ver_user['role']

            # set session
            request.session['user_login'] = username
            request.session['access_token'] = access_token
            request.session['kode_identitas'] = kode_identitas
            request.session['role'] = role
            messages.success(request, "Login success.")
        else:
            messages.warning(request, "Wrong username or password.")
    return HttpResponseRedirect(request.META.get('HTTP_REFERER','/'))

def hapus_data(request):
    try:
        Mahasiswa.objects.all().delete()
    except:
        pass
    return HttpResponseRedirect(reverse('app_login:index'))