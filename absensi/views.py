from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib import messages
import time
from .models import *

response = {}
def index(request):

    if not 'user_login' in request.session.keys():

        return HttpResponseRedirect(reverse('app_login:index'))
    else:
        mahasiswa_list = Mahasiswa.objects.filter(npm=request.session["kode_identitas"])
        response["mahasiswa_list"] = mahasiswa_list
        return render(request, 'index.html', response)

def auth_logout(request):
    request.session.flush() # clear all session
    messages.warning(request, "Logout successful")
    return HttpResponseRedirect(request.META.get('HTTP_REFERER','/'))

def catat_absen(request, npm):
    try :
        mahasiswa = Mahasiswa(npm=npm, waktu=time.ctime())
        mahasiswa.save()
        return HttpResponse(status=200)
    except:
        return HttpResponse(status=500)
