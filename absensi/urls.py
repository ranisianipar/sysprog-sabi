from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^logout/$', auth_logout, name='logout'),
    url(r'^catat/(?P<npm>[0-9]+)', catat_absen, name='catat')
]
