"""sabi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include
from django.views.generic import RedirectView
import app_login.urls as app_login
import absensi.urls as absensi

urlpatterns = [
    url('admin/', admin.site.urls),
    url(r'^absensi/', include((absensi, 'absensi'), namespace='absensi')),
    url(r'^app_login/', include((app_login, 'app_login'), namespace='app_login')),
    url(r'^$', RedirectView.as_view(url='app_login/', permanent=True), name='index'),
]
