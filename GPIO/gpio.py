import RPi.GPIO as GPIO
from RPLCD import CharLCD
from record import rekam
import time
from voiceit2 import *
from perbesar import perbesar
from threading import Thread
import os
import requests 

#set mode gpio
GPIO.setmode(GPIO.BOARD)

# lampu
#set gpio yang akan digunakan untuk menandakan bahwa absen tercatat	
GPIO.setup(11,GPIO.OUT)
#set gpio yang akan digunakan untuk menandakan bahwa absen gagal
GPIO.setup(13,GPIO.OUT)
#set gpio yang akan digunakan untuk menandakan proses recording sedang berjalan
GPIO.setup(15, GPIO.OUT)

# tombol
#set gpio yang akan digunakan sebagain button 
GPIO.setup(12,GPIO.IN, pull_up_down = GPIO.PUD_UP)

# servo
#set gpio yang akan digunakan sebagai servo motor (referensi: https://www.instructables.com/id/Servo-Motor-Control-With-Raspberry-Pi/)
GPIO.setup(16, GPIO.OUT)

# lcd
GPIO.setup(33, GPIO.OUT)
GPIO.setup(31, GPIO.OUT)
GPIO.setup(29, GPIO.OUT)
GPIO.setup(23, GPIO.OUT)

GPIO.setup(35, GPIO.OUT)
GPIO.setup(37, GPIO.OUT)

# blinking function
def blink (pin):
	GPIO.output(pin,GPIO.HIGH)

#blinkoff function
def blinkoff (pin):
	GPIO.output(pin,GPIO.LOW)

#fungsi LED yang dijalankan ketika absensi tercatat
def isPresent():
	blink(11)
	time.sleep(1)
	blinkoff(11)
	time.sleep(1)

#fungsi LED yang dijalankan ketika absensi tidak tercatat	
def notPresent():
	blink(13)
	time.sleep(1)
	blinkoff(13)
	time.sleep(1)

#fungsi LED yang dijalankan ketika merekam 	
def recording():
	rekam()

	for i in range(2):
		blink(15)
		time.sleep(0.5)
		blinkoff(15)
		time.sleep(0.5)
	
# jeremy = 1606917576
# kevin  = 1606835595

daftar = { '1606917576' : 'usr_528daa539515477b841dd6a36c9cd7ac', '1606835595' : 'usr_631dfef7e24345ada752a1d583b27a7e' }
orang = { '1606917576' : u'jeremy', '1606835595' : u'kevin'}

VI_KEY = 'key_d54ca274a5fc481594fc14cc003c0439'
VI_TOKEN = 'tok_a0faa85723a94ecaa411102b23f7dd6e'
my_voiceit = VoiceIt2(VI_KEY, VI_TOKEN)
#fungsi yang dijalankan untuk perintah push button 
def button():
	while True:
		button_state = GPIO.input(12)
		if button_state == False:
			recording()
			time.sleep(0.1)
			# verifikasi
			print('verifikasi')
			jeremy = my_voiceit.voice_verification(daftar['1606917576'], 'en-US', 'my face and voice identify me', 'suara.wav')
			kevin = my_voiceit.voice_verification(daftar['1606835595'], 'en-US', 'my face and voice identify me', 'suara.wav')

			print(jeremy)
			print(kevin)

			npm = ''
			if jeremy['responseCode'] == 'SUCC':
				npm = '1606917576'
				isPresent()
				nama = orang[npm]
				LCD(nama)
				servo()
			elif kevin['responseCode'] == 'SUCC':
				npm = '1606835595'
				isPresent()
				nama = orang[npm]
				LCD(nama)
				servo()
			else:
				notPresent()	
				LCD(u'coba lagi')	
			
			# npm = ''
			# if jeremy['responseCode'] == 'SUCC':
			# 	url = "https://sabimacrosoft.herokuapp.com/absensi/catat/1606917576"
			# 	r = requests.get(url)
			# 	npm = '1606917576'
			# elif kevin['responseCode'] == 'SUCC':
			# 	url = "https://sabimacrosoft.herokuapp.com/absensi/catat/1606835595"
			# 	r = requests.get(url)
			# 	npm = '1606835595'
			# else:
			# 	r = 0

			# try :
			# 	if r.status_code == 200 :
			# 		isPresent()
			# 		nama = orang[npm]
			# 		LCD(nama)
			# 		servo()
			# 		print('masuk')
			# 	else :
			# 		print('masuk2')
			# 		notPresent()
			# 		LCD(u'coba lagi')
			# except:
			# 	notPresent()
			# 	LCD(u'coba lagi')
		
			os.remove('suara.wav')
	print('selesai')


#fungsi untuk servo
def servo():
	pwm = GPIO.PWM(16, 50)
	pwm.start(7.5)
	pwm.ChangeDutyCycle(7.5)        #90 derajat
	time.sleep(1)                   #set disini berapa lama pintu ngebuka
	pwm.ChangeDutyCycle(12.5)          #nutup lagi
	time.sleep(1) 
	pwm.stop()


def LCD(nama):
	lcd = CharLCD(cols=16, rows=2, pin_rs=37, pin_e=35, pins_data=[33, 31, 29, 23])
	lcd.clear() 			# untuk ngehapus tulisan di lcd 
	lcd.write_string(nama)
	lcd.cursor_pos = (1,3) #isilah untuk posisi tulisannya bagian mana
	time.sleep(1)          #isi untuk menentukan berapa lama ketulis
 		 
######
def main():
	button()
######
                
if __name__ == '__main__':
	main()
	GPIO.cleanup()


		
